import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { EnvironmentConfig } from './environment-config';

@Injectable({
  providedIn: 'root',
})
export class EnvironmentConfigService {
  configuration!: EnvironmentConfig;
  configFilename = 'assets/profiles/environmentConfig.json';

  constructor(private httpClient: HttpClient) {}

  async loadEnvironmentConfig() {
    this.configuration = await firstValueFrom(
      this.httpClient.get<EnvironmentConfig>(this.configFilename)
    );
  }
}
