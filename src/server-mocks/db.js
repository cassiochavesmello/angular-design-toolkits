const Path = require('path');
const glob = require('glob');
const apiFiles = glob.sync(Path.resolve(__dirname, './') + '/**/*.json', {
  nodir: true,
});

let data = {};

apiFiles.forEach((filePath) => {
  const api = require(filePath);
  let [, url] = filePath.split('server-mocks/'); // e.g. comments.js
  url = url.slice(0, url.length - 5); // remove .js >> comments
  data[url] = api;
  console.log(data);
});

// data will be :
// { 'comments': [ { id: 1, body: 'some comment', postId: 1 } ],
//   'db': {},
//   'posts': [ { id: 1, title: 'json-server', author: 'typicode' } ],
//   'profile': { name: 'typicode' } }

module.exports = () => {
  return data;
};
