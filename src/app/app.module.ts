import { CoreModule } from './core/core.module';
import { HttpClientModule } from '@angular/common/http';
import { EnvironmentConfigService } from './../environments/configuration/environment-config.service';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { routes } from './app.routes';
import { HomeComponent } from './base/home/home.component';
import { HeaderComponent } from './base/header/header.component';
import { FooterComponent } from './base/footer/footer.component';

const appInitializerFn = (environmentConfig: EnvironmentConfigService) => {
  return () => {
    environmentConfig.loadEnvironmentConfig();
  };
};

@NgModule({
  declarations: [AppComponent, HomeComponent, HeaderComponent, FooterComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    CoreModule,
  ],
  providers: [
    EnvironmentConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFn,
      multi: true,
      deps: [EnvironmentConfigService],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
