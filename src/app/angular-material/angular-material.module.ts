import { RouterModule } from '@angular/router';
import { ItemService } from './services/item.service';
import { AccordionComponent } from './components/accordion/accordion.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { angularMaterialRoutes } from './angular-material.routes';
import { ComponentsListComponent } from './components/components-list/components-list.component';

@NgModule({
  declarations: [AccordionComponent, ComponentsListComponent],
  imports: [CommonModule, RouterModule.forChild(angularMaterialRoutes)],
  providers: [ItemService],
})
export class AngularMaterialModule {}
