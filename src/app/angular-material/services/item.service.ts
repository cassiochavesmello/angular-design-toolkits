import { EnvironmentConfigService } from './../../../environments/configuration/environment-config.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Item } from '../model/item';

@Injectable({
  providedIn: 'root',
})
export class ItemService {
  baseUrl = '';

  constructor(
    private httpClient: HttpClient,
    private envConfigService: EnvironmentConfigService
  ) {
    this.baseUrl = this.envConfigService.configuration.backendUrl;
  }

  getAllItens(): Observable<Item[]> {
    return this.httpClient.get<Item[]>(`${this.baseUrl}/itens`);
  }
}
