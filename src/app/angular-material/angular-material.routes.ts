import { AccordionComponent } from './components/accordion/accordion.component';
import { ComponentsListComponent } from './components/components-list/components-list.component';

export const angularMaterialRoutes = [
  { path: '', component: ComponentsListComponent },
  { path: 'accordion', component: AccordionComponent },
];
