import { ItemService } from './../../services/item.service';
import { Component, OnInit } from '@angular/core';
import { Item } from '../../model/item';

@Component({
  selector: 'ngm-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss'],
})
export class AccordionComponent implements OnInit {
  itens: Item[] = [];

  constructor(private readonly itemService: ItemService) {}

  ngOnInit(): void {
    this.getItens();
  }

  getItens() {
    this.itemService.getAllItens().subscribe((allItens) => {
      this.itens = allItens;
    });
  }
}
