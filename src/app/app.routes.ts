import { HomeComponent } from './base/home/home.component';

export const routes = [
  { path: '', component: HomeComponent },
  {
    path: 'angular-material',
    loadChildren: () =>
      import('./angular-material/angular-material.module').then(
        (m) => m.AngularMaterialModule
      ),
  },
];
