import { ServiceUnavailableComponent } from './components/service-unavailable/service-unavailable.component';
import { AccessDeniedComponent } from './components/access-denied/access-denied.component';

export const coreRoutes = [
  { path: '403', component: AccessDeniedComponent },
  { path: '503', component: ServiceUnavailableComponent },
];
