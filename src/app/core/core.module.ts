import { RouterModule } from '@angular/router';
import { GlobalErrorHandler } from './handler/global-error.interceptor';
import { GlobalHttpHandler } from './handler/global-http.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { coreRoutes } from './core.routes';
import { AccessDeniedComponent } from './components/access-denied/access-denied.component';
import { ServiceUnavailableComponent } from './components/service-unavailable/service-unavailable.component';

@NgModule({
  declarations: [AccessDeniedComponent, ServiceUnavailableComponent],
  imports: [CommonModule, RouterModule.forRoot(coreRoutes)],
  providers: [
    { provide: ErrorHandler, useClass: GlobalErrorHandler },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: GlobalHttpHandler,
      multi: true,
    },
  ],
})
export class CoreModule {}

