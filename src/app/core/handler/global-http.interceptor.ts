import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Router } from '@angular/router';
import { catchError, Observable, throwError, timeout } from 'rxjs';

@Injectable()
export class GlobalHttpHandler implements HttpInterceptor {
  constructor(private readonly router: Router) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      timeout(10000),
      catchError((error) => this.handleHttpError(error))
    );
  }

  handleHttpError = (response: any) => {
    if (response instanceof HttpErrorResponse) {
      console.log(response.error);
      switch (this.getHttpStatusResponseClass(response.status)) {
        case '4':
          this.router.navigate(['403']); // Forbidden
          break;
        case '5':
          this.router.navigate(['503']); // Service Unavailable
          break;
        default: // Home
          this.router.navigate(['']);
          break;
      }
    }
    return throwError(() => new HttpErrorResponse(response));
  };

  getHttpStatusResponseClass(httpStatus: number) {
    return String(httpStatus)[0];
  }
}
